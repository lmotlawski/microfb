/*jshint node: true */
var passport = require('passport');
var User = require('./models/user');
var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  if(!req.user){
    res.sendFile(__dirname + '/public/index.html');
  } else {
    res.redirect('/fb');
  }
});

router.get('/register', function(req, res) {
  if(!req.user){
    res.sendFile(__dirname + '/public/register.html');
  } else {
    res.redirect('/fb');
  }
});

router.post('/register', function(req, res, next) {
  User.register(new User({email: req.body.email, firstName: req.body.firstName, lastName: req.body.lastName}), req.body.password, function(err) {
    if (err) {
      return next(err);
    }
    res.send({redirect: '/'});
  });
});

router.get('/login', function(req, res) {
  if(!req.user){
    res.sendFile(__dirname + '/public/index.html');
  } else {
    res.redirect('/fb');
  }
});

router.post('/login', passport.authenticate('local'), function(req, res) {
  res.send({redirect: '/fb'});
});

router.get('/fb', function(req, res) {
  if(req.user){
    res.sendFile(__dirname + '/public/fb.html');
  } else {
    res.redirect('/');
  }
});

router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});

module.exports = router;
