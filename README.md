# Purpose of the project: 
Final project for Web Technologies course.

# MicroFB:
- Registration and login in
- Adding friends
- Chat and user boards
- Creating posts
- Following other users
- Ability to like posts
- Online statuses and notifications 

# Used technologies:
- JavaScript
- Socket.IO
- Node.js
- MongoDB