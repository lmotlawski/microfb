var express  = require('express');
var app      = express();
var path = require('path');
var http     = require('http');
var socketio = require('socket.io');
var passportSocketIo = require('passport.socketio');
var port     = process.env.PORT || 3000;
var mongoose = require('mongoose');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var MongoStore   = require('connect-mongo')(session);

var server   = http.createServer(app);
var io       = socketio.listen(server);

var url = 'mongodb://localhost/test';
var db = mongoose.connect(url);
var sessionStore = new MongoStore({ mongooseConnection: db.connection });
var User = require('./models/user');
var Statement = require('./models/statement');
var Message = require('./models/message');
var Post = require('./models/post');

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(session({
  key: 'connect.sid',
  secret: 'secret',
  store: sessionStore,
  resave: true,
  saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

var User = require('./models/user');
passport.use(new LocalStrategy(User.authenticate()));

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(express.static(__dirname + '/public'));

//Routes
var router = require('./routes');
app.use('/', router);


io.use(passportSocketIo.authorize({
  passport:     passport,
  cookieParser: cookieParser,
  key:          'connect.sid',
  secret:       'secret',
  store:        sessionStore,
  success:      onAuthorizeSuccess,
  fail:         onAuthorizeFail
}));

function onAuthorizeSuccess(data, accept){
  accept(null, true);
}

function onAuthorizeFail(data, message, error, accept){
  if(error)
    throw new Error(message);
  console.log('failed connection to socket.io:', message);
  accept(null, false);
}

var onlineUsers = [];

io.sockets.on('connection', function(socket) {
  socket.join(socket.request.user._id);
  socket.emit('userData', socket.request.user);

  Statement.find({to: socket.request.user._id}, function(error, statements){
    if(statements){
      socket.emit('statements', statements);
    }
  });

  var sendFriendsList = function(user){
    User.find({_id: {$in: user.friends}}, function(error,friends){
      if(error) throw error;
      io.sockets.in(user._id).emit('friendsList', friends);
      if(onlineUsers.indexOf(socket.request.user._id.toString()) === -1){
        onlineUsers.push(""+socket.request.user._id+"");
        io.emit('onlineUsers', onlineUsers);
      } else {
        io.emit('onlineUsers', onlineUsers);
      }
    }).sort([['lastName', 1]]);
  };
  sendFriendsList(socket.request.user);


  var getPosts = function(user){
    Post.find({author: user}, function(error, posts){
      if(error) throw error;
      if(user !== socket.request.user._id){
        User.find({_id: socket.request.user._id}, function(error, loggedUser){
          if (error) throw error;
          var result, data;
          if(loggedUser[0].following.indexOf(user) > -1){
            result = 1;
          } else {
            result = 0;
          }
          data = {
            posts: posts,
            result: result
          };
          socket.emit('posts', data);
        });
      }
      socket.emit('posts', posts);
    }).sort([['date', 1]]);
  };

  var getMyWallPosts = function(){
    User.find({_id: socket.request.user._id}, function(error, user){
      if(error) throw error;
      user[0].following.push(socket.request.user._id);
      Post.find({author: {$in: user[0].following}}, function(error, posts){
        if(error) throw error;
        socket.emit('posts', posts);
      }).sort([['date', 1]]);
    });
  };
  getMyWallPosts();

  socket.on('getPosts', function(id){
    getPosts(id);
  });

  socket.on('getMyWallPosts', function(){
    getMyWallPosts();
  });

  socket.on('invitationResponse', function(data){
    if(data.status === 1){
      Statement.find({_id: data.id}, function(error, statement){
        if(error) throw error;
        Statement.remove({to: statement[0].to, from: statement[0].from}, function(error) {
          if(error) throw error;
        });
        User.find({_id: statement[0].to}, function(error, user){
          if(error) throw error;
          user[0].friends.push(statement[0].from);
          user[0].save(function(error) {
            if (error) {
              throw error;
            }
            sendFriendsList(user[0]);
          });
        });
        User.find({_id: statement[0].from}, function(error, user){
          if(error) throw error;
          user[0].friends.push(statement[0].to);
          user[0].save(function(error) {
            if (error) {
              throw error;
            }
            sendFriendsList(user[0]);
          });
        });
      });
    } else {
      Statement.remove({_id: data.id}, function(error) {
        if(error) throw error;
      });
    }
  });

  socket.on('searchUser', function(data){
    User.find({email: data}, function(error, user){
      if(error) throw error;
      if(user.length !== 0){
        if(user[0].friends.indexOf(socket.request.user._id) === -1){
          socket.emit('foundUser', user);
        } else {
          var data = {
            user: user,
            friend: 1
          };
          socket.emit('foundUser', data);
        }
      }
    });
  });

  socket.on('addFriend', function(data){
    User.find({_id: data}, function(error, user){
      if(error) throw error;
      if(user[0].friends.indexOf(data) === -1){
        Statement.find({type: 'invitation', from: socket.request.user._id, to: data}, function(error, statements){
          if(statements.length === 0){
            var newStatement = new Statement({type: 'invitation', from: socket.request.user._id, fromText: socket.request.user.firstName + " " + socket.request.user.lastName, to: data});
            newStatement.save(function(error) {
              if (error) throw error;
              Statement.find({type: 'invitation', from: socket.request.user._id, fromText: socket.request.user.firstName + " " + socket.request.user.lastName, to: data}, function(error, statement){
                io.sockets.in(data).emit('newInvitation', statement);
              });
            });
          }
        });
      }
    });
  });

  socket.on('removeFriend', function(id){
    var index;
    User.find({_id: socket.request.user._id}, function(error, user1){
      if(error) throw error;
      index = user1[0].friends.indexOf(id);
      user1[0].friends.splice(index, 1);
      if(user1[0].following.indexOf(id) > -1){
        index = user1[0].following.indexOf(id);
        user1[0].following.splice(index, 1);
      }
      user1[0].save(function(error) {
        if (error) throw error;
        sendFriendsList(user1[0]);
        getMyWallPosts();
        User.find({_id: id}, function(error, user2){
          if(error) throw error;
          index = user2[0].friends.indexOf(socket.request.user._id);
          user2[0].friends.splice(index, 1);
          if(user2[0].followedBy.indexOf(socket.request.user._id) > -1){
            index = user2[0].followedBy.indexOf(socket.request.user._id);
            user2[0].followedBy.splice(index,1 );
          }
          user2[0].save(function(error) {
            if (error) throw error;
            sendFriendsList(user2[0]);
            io.sockets.in(id).emit('getMyWall', socket.request.user._id);
          });
        });
      });
    });
  });

  socket.on('deleteStatement', function(id){
    Statement.remove({from: id, to: socket.request.user._id}, function(error){
      if(error) throw error;
    });
  });

  socket.on('loadChatHistory', function(data){
    Statement.remove({from: data, to: socket.request.user._id}, function(error) {
      if(error) throw error;
    });
    Message.find({ $or: [ {from: socket.request.user._id, to: data}, {from: data, to: socket.request.user._id} ] }, function(error, history){
      if(error) throw error;
      socket.emit('history', history);
    }).sort([['date', 1]]);
  });

  socket.on('sendMessage', function(message){
    var newMessage = new Message({from: socket.request.user._id, to: message.to, text: message.text});
    newMessage.save(function(error){
      if (error) throw error;
      var newStatement = new Statement({type: 'message', from: socket.request.user._id, to: message.to});
      newStatement.save(function(error){
        if (error) throw error;
        socket.emit('newMessage', newMessage);
        io.sockets.in(message.to).emit('newMessage', newMessage);
      });
    });
  });

  socket.on('sendMessageToFriends', function(message){
    var id = socket.request.user._id;
    User.find({_id: id}, function(error, user){
      if (error) throw error;
      for(i=0; i<user[0].friends.length; i++){
        var newMessage = new Message({from: socket.request.user._id, to: user[0].friends[i], text: message});
        newMessage.save(function(error){
          if (error) throw error;
        });
        var newStatement = new Statement({type: 'message', from: socket.request.user._id, to: user[0].friends[i]});
        newStatement.save(function(error){
          if (error) throw error;
        });
        io.sockets.in(user[0].friends[i]).emit('newMessage', newMessage);
      }
    });
  });

  socket.on('sendPost', function(data){
    var newPost = new Post({description: data.description, text: data.text, author: data.id});
    newPost.save(function (error){
        if(error) throw error;
        if(data.id === socket.request.user._id.toString()){
          getMyWallPosts();
        } else {
          getPosts(data.id);
        }
    });
    User.find({_id: data.id}, function(error, user){
      if (error) throw error;
      user[0].followedBy.push(data.id);
      io.emit('refreshWall', user[0].followedBy);
    });
  });

  socket.on('deletePost', function(data){
    Post.remove({_id: data.idPost}, function(error){
      if (error) throw error;
      User.find({_id: data.idWall}, function(error, user){
        if (error) throw error;
        user[0].followedBy.push(data.idWall);
        io.emit('refreshWall', user[0].followedBy);
      });
    });
  });

  socket.on('follow', function(id){
    User.find({_id: socket.request.user._id}, function(error, user){
      if (error) throw error;
      user[0].following.push(id);
      user[0].save(function(error) {
        if (error) throw error;
      });
      User.find({_id: id}, function(error, user1){
        if (error) throw error;
        user1[0].followedBy.push(user[0]._id);
        user1[0].save(function(error){
          if (error) throw error;
        });
      });
    });
  });

  socket.on('unfollow', function(id){
    User.find({_id: socket.request.user._id}, function(error, user){
      if (error) throw error;
      var index = user[0].following.indexOf(id);
      user[0].following.splice(index, 1);
      user[0].save(function(error) {
        if (error) throw error;
      });
      User.find({_id: id}, function(error, user1){
        if (error) throw error;
        var index = user1[0].followedBy.indexOf(user[0]._id);
        user1[0].followedBy.splice(index, 1);
        user1[0].save(function(error) {
          if (error) throw error;
        });
      });
    });
  });

  socket.on('sharePost', function(post){
    Post.find({description: post.description, text: post.text, author: post.author, date: post.date}, function(error, foundPost){
      if (error) throw error;
      if(foundPost[0]){
      } else {
        var newPost = new Post({description: post.description, text: post.text, author: post.author, date: post.date});
        newPost.save(function (error){
            if(error) throw error;
        });
      }
      User.find({_id: post.author}, function(error, user){
        if (error) throw error;
        user[0].followedBy.push(post.author);
        io.emit('refreshWall', user[0].followedBy);
      });
    });
  });

  socket.on('likePost', function(like){
    Post.find({_id: like.idPost}, function(error, foundPost){
      if (error) throw error;
      if(foundPost[0].likes.indexOf(like.idUser) === -1){
        foundPost[0].likes.push(like.idUser);
        foundPost[0].save(function(error) {
          if (error) throw error;
        });
      } else {
        var index = foundPost[0].likes.indexOf(like.idUser);
        foundPost[0].likes.splice(index, 1);
        foundPost[0].save(function(error) {
          if (error) throw error;
        });
      }
      User.find({_id: foundPost[0].author}, function(error, user){
        if (error) throw error;
        user[0].followedBy.push(foundPost[0].author);
        io.emit('refreshWall', user[0].followedBy);
      });
    });
  });

  socket.on('disconnect', function() {
      var index = onlineUsers.indexOf(socket.request.user._id.toString());
      onlineUsers.splice(index, 1);
      socket.broadcast.emit('onlineUsers', onlineUsers);
   });
});

server.listen(3000, function() {
  console.log('App running on port: ' + port);
});
