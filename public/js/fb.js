$(document).ready(function () {
  var socket = io();

  $('#button-search-user').on("click", function(e) {
    e.preventDefault();
    var data = $('#user-data-search').val();
    if($('#wall-title').attr('email') !== data){
      socket.emit('searchUser', data);
    }
    $('#user-data-search').val('');
  });

  $(document).on("click", '#addFriend', function() {
    var id = $(this).prev('input').val();
    socket.emit('addFriend', id);
    $('#searchData').html('');
  });

  $(document).on("click", '#removeFriend', function(e) {
    e.stopImmediatePropagation();
    var id = $('#wall-title').attr('idWall');
    if(id === $('.active').attr('idFriend')){
      $('#chatbox').text('');
    }
    $('#wall-title').html('<div id="wallUser">' + $('#loggedUser').val() + '</div>');
    $('#wall-title').attr('idWall', id);
    $('#comment').val('');
    socket.emit('removeFriend', id);
  });

  $(document).on("click", '#acceptInvitation', function() {
    var idS = $(this).prevAll('input').val();
    $(this).parent().remove();
    socket.emit('invitationResponse', {id: idS, status: 1});
  });

  $(document).on("click", '#declineInvitation', function() {
    var idS = $(this).prevAll('input').val();
    $(this).parent().remove();
    socket.emit('invitationResponse', {id: idS, status: 0});
  });

  $(document).on("click", '.fill-div', function() {
    if($(".active").length > 0){
      $(".active").removeClass("active");
    }
    $(this).parent().addClass('active');
    $(this).find('.message').find('img').remove();
    var idTo = $('.active').attr('idFriend');
    socket.emit('loadChatHistory', idTo);
  });

  $(document).on("click", '#backToMyWall', function() {
    var id = $('#loggedUser').attr('idUser');
    $('#wall-title').html('<div id="wallUser">' + $('#loggedUser').val() + '</div>');
    $('#wall-title').attr('idWall', id);
    $('#comment').val('');
    socket.emit('getMyWallPosts');
  });

  $(document).on("click", '#getPosts', function() {
    var id = $(this).prev('input').val();
    $('#wall-title').attr('idWall', id);
    var back = $('<div id="back"></div>'),
      wallUser = $('<div id="wallUser"></div>'),
      backButton = $('<div id="backButton">Back to my Wall<button type=submit class="btn btn-success glyphicon glyphicon-triangle-left" id="backToMyWall"></button></div>');
    wallUser.append('<div>' + $('#user').text() + '</div>');
    back.append(wallUser).append(backButton);
    $('#wall-title').html(back);
    $('#foundUsers').html('');
    $('#comment').val('');
    socket.emit('getPosts', id);
  });

  $(document).on("click", '#deletePost', function() {
    var id = $(this).parent().parent().attr('idPost'),
      idWall = $('#wall-title').attr('idWall'),
      data;
    data = {
      idPost: id,
      idWall: idWall
    };
    socket.emit('deletePost', data);
  });

  $(document).on("click", '#follow', function() {
    var id = $('#wall-title').attr('idWall'),
      button = $('<div id="followLabel">Unfollow</div><button type=submit class="navbar-right btn btn-danger glyphicon glyphicon-eye-close" id="unfollow" data-toggle="tooltip" data-placement="right" title="Unfollow user"></button>');
    $('#followButton').html(button);
    socket.emit('follow', id);
  });

  $(document).on("click", '#unfollow', function() {
    var id = $('#wall-title').attr('idWall'),
      button = $('<div id="followLabel">Follow</div><button type=submit class="navbar-right btn btn-success glyphicon glyphicon-eye-open" id="follow" data-toggle="tooltip" data-placement="right" title="Follow user"></button>');
    $('#followButton').html(button);
    socket.emit('unfollow', id);
  });

  $(document).on("click", '#sharePost', function() {
    var post = {
      description: "[Shared] "+$(this).next().text(),
      text: $(this).parent().next().text(),
      author: $('#loggedUser').attr('idUser'),
      date: $(this).parent().parent().attr('datePost')
    };
    socket.emit('sharePost', post);
  });

  $(document).on("click", '#likePost', function() {
    var likePost= {
      idPost: $(this).parent().parent().attr('idPost'),
      idUser: $('#loggedUser').attr('idUser'),
      idWall: $('#wall-title').attr('idWall')
    };
    socket.emit('likePost', likePost);
  });

  $(document).on("click", '#buttonWall', function() {
    $('#wall-title').attr('idWall', id);
    var back = $('<div id="back"></div>'),
      wallUser = $('<div id="wallUser"></div>'),
      backButton = $('<div id="backButton">Back to my Wall<button type=submit class="btn btn-success glyphicon glyphicon-triangle-left" id="backToMyWall"></button></div>');
    wallUser.append('<div>' + $('#user').text() + '</div>');
    back.append(wallUser).append(backButton);
    $('#wall-title').html(back);
    $('#foundUsers').html('');
    $('#comment').val('');
    var id = $('#buttonWall').parent().parent().attr('class');
    socket.emit('getPosts', id);
  });

  $('#form-send-message').submit(function (e) {
    e.preventDefault();
    if($('#usermsg').val() !== ""){
      var idTo = $('.active').attr('idFriend'),
        toAll = $('.active').find('#toAll'),
        message;
      if(idTo){
        message = {
          to: idTo,
          text: $('#usermsg').val()
        };
        $('#usermsg').val('');
        socket.emit('sendMessage', message);
      } else if(toAll) {
        message = $('#usermsg').val();
        var chatbox = $('#chatbox');
        var container = $('<div id="messageChat">'+"Message to all friends: "+$('#usermsg').val()+'</div>');
        chatbox.append(container);
        var scroll = $('#chatbox');
        var height = scroll[0].scrollHeight;
        scroll.scrollTop(height);
        $('#usermsg').val('');
        socket.emit('sendMessageToFriends', message);
      }
    }
  });

  $('#form-send-post').submit(function (e) {
    e.preventDefault();
    var sender;
    if($('#wallUser').text() === $('#loggedUser').val()){
      sender = $('#loggedUser').val();
    } else {
      sender = $('#loggedUser').val() + " wrote to " + $('#wallUser').text();
    }
    var data = {
      description: sender,
      text: $('#comment').val(),
      id: $('#wall-title').attr('idWall')
    };
    $('#comment').val('');
    socket.emit('sendPost', data);
  });

  socket.on('userData', function(data){
    $('#loggedUser').text("Logged as " + data.firstName + " " + data.lastName + " (" + data.email + ")");
    $('#loggedUser').attr('idUser', data._id);
    $('#loggedUser').val(data.firstName + " " + data.lastName);
    $('#wall-title').attr('idWall', data._id);
    $('#wall-title').attr('email', data.email);
    $('#wall-title').html('<div id="wallUser">' + $('#loggedUser').val() + '</div>');
  });

  socket.on('foundUser', function(data){
    var container, input, user, button;
    if(data.friend !== 1){
      if(data.length !== 0){
        $('#foundUsers').text('');
          container = $('<div id="searchData"></div>');
          input = $('<input type="hidden" value="'+data[0]._id+'">');
          user = $('<div id="user"></div>');
          button = $('<button type=submit class="btn btn-success glyphicon glyphicon-plus" id="addFriend" data-toggle="tooltip" data-placement="right" title="Add user to friends list"></button>');
          user.text(data[0].firstName + " " + data[0].lastName);
        container.append(user).append(input).append(button);
        $('#foundUsers').append(container);
      } else {
        $('#foundUsers').text('');
      }
    } else {
      $('#foundUsers').text('');
        container = $('<div id="searchData"></div>');
        input = $('<input type="hidden" value="'+data.user[0]._id+'">');
        user = $('<div id="user"></div>');
        button = $('<button type=submit class="btn btn-success glyphicon glyphicon-triangle-right" id="getPosts" data-toggle="tooltip" data-placement="right" title="Get user wall"></button>');
        user.text(data.user[0].firstName + " " + data.user[0].lastName);
      container.append(user).append(input).append(button);
      $('#foundUsers').append(container);
    }
  });

  socket.on('statements', function(statements){
    var invitations = $('.invitations');
    for(i=0; i<statements.length; i++){
      if(statements[i].type === "invitation"){
        var input = $('<input type="hidden" value="'+statements[i]._id+'">'),
          container = $('<div id="invitation"></div>'),
          from = $('<div id="from"></div>'),
          buttonY = $('<button type=submit class="btn btn-success glyphicon glyphicon-ok" id="acceptInvitation" data-toggle="tooltip" data-placement="right" title="Accept invitation"></button>'),
          buttonN = $('<button type=submit class="btn btn-success glyphicon glyphicon-remove" id="declineInvitation" data-toggle="tooltip" data-placement="right" title="Decline invitation"></button></br>');
        from.text(statements[i].fromText);
        container.append(input).append(from).append(buttonY).append(buttonN);
        invitations.append(container);
      } else if(statements[i].type === "message"){
        var statement = statements[i];
        setTimeout(function() {
          if ($('.'+statement.from).length > 0) {
              $('.'+statement.from).find('.message').html('<img id="messageImg" src="img/message.png">');
          }
        },1);
      }
    }
  });

  socket.on('newInvitation', function(statement){
    var invitations = $('.invitations'),
      container = $('<div id="invitation"></div>'),
      input = $('<input type="hidden" value="'+statement[0]._id+'">'),
      user = $('<div id="user"></div>'),
      buttonY = $('<button type=submit class="btn btn-success glyphicon glyphicon-ok" id="acceptInvitation" data-toggle="tooltip" data-placement="right" title="Accept invitation"></button>'),
      buttonN = $('<button type=submit class="btn btn-success glyphicon glyphicon-remove" id="declineInvitation" data-toggle="tooltip" data-placement="right" title="Decline invitation"></button></br>');
    user.text(statement[0].fromText);
    container.append(input).append(user).append(buttonY).append(buttonN);
    invitations.append(container);
  });

  socket.on('friendsList', function(friends){
    var friendsList = $('.friendsList');
    $('.friendsList').text('');
    for(i=0; i<=friends.length; i++){
      if(i<friends.length){
        var container = $('<div id="friend" class="'+friends[i]._id+'"></div>'),
          a = $('<a href="#" class="fill-div"></a>'),
          input = $('<input type="hidden" value="'+friends[i]._id+'">'),
          user = $('<div id="user"></div>'),
          status = $('<div class="status"></div>'),
          message = $('<div class="message"></div>'),
          getWall = $('<div class="getWallButton"></div>'),
          buttonWall = $('<button type=submit class="btn btn-default glyphicon glyphicon-globe" id="buttonWall" data-toggle="tooltip" data-placement="right" title="Accept invitation"></button>');

        container.attr('idFriend', friends[i]._id);
        getWall.append(buttonWall);
        user.text(friends[i].firstName + " " + friends[i].lastName);
        a.append(input).append(user).append(status).append(message);
        container.append(getWall).append(a);
        friendsList.append(container);
      } else {
        var footer = $('<div class="panel-footer" id="messageToFriends"></div>'),
          aFooter = $('<a href="#" class="fill-div"></a>'),
          toAll = $('<div id="toAll">Send message to all friends</div>');
        aFooter.append(toAll);
        footer.append(aFooter);
        friendsList.append(footer);
      }
    }
  });

  socket.on('history', function(history){
    var chatbox = $('#chatbox');
    $('#chatbox').text('');
    for(i=0; i<history.length; i++){
      var container = $('<div class="messageChat"></div>'),
        from;
      if(history[i].from === $(".active").children().children().val()){
        from = $(".active").children().children().next().text() + ": ";
      } else {
        from = "Me: ";
      }
      container.append(from + history[i].text);
      chatbox.append(container);
      var scroll = $('#chatbox');
      var height = scroll[0].scrollHeight;
      scroll.scrollTop(height);
    }
  });

  socket.on('newMessage', function(message){
    var idFrom = $('.active').attr('idFriend');
    if(idFrom === message.from || idFrom === message.to){
      var chatbox = $('#chatbox'),
       container = $('<div id="messageChat"></div>'),
       from;
      if(message.from === $(".active").children().children().val()){
        from = $(".active").children().children().next().text() + ": ";
      } else {
        from = "Me: ";
      }
      container.append(from + message.text);
      chatbox.append(container);
      var scroll = $('#chatbox');
      var height = scroll[0].scrollHeight;
      scroll.scrollTop(height);
      socket.emit('deleteStatement', message.from);
    } else {
      $('.'+message.from).find('.message').html('<img id="messageImg" src="img/message.png">');
    }
  });

  socket.on('posts', function(post){
    var posts = $('#posts');
    $('#posts').text('');
    var container, shareButton, deleteButton, heading, author, date, text, postDate, likeButton, likes;
    if(post.result === undefined){
      for(i=0; i<post.length; i++){
        if(post[i].likes.length === 0){
          likes = "";
        } else if (post[i].likes.length === 1) {
          likes = "1 like";
        } else {
          likes = post[i].likes.length + " likes";
        }
        container = $('<div class="panel panel-default posts" idPost="'+post[i]._id+'"></div>');
        deleteButton = $('<button type=submit class="navbar-right btn btn-danger glyphicon glyphicon-trash" id="deletePost" data-toggle="tooltip" data-placement="right" title="Delete post"></button>');
        likeButton = $('<div class="panel-footer" id="likeButton"><button type=submit class="btn btn-info glyphicon glyphicon-thumbs-up" id="likePost"></button><div id="likes">'+likes+'</div></div>');
        heading = $('<div class="panel-heading author"></div>');
        author = $('<div id="author">'+post[i].description+'</div>');
        text = $('<div id="post">'+post[i].text+'</div>');
        date = post[i].date;
        date = date.split('T');
        date[1] = date[1].split('.');
        date = date[0] + " " + date[1][0];
        postDate = $('<div id="date">' + date + '</div>');
        if(post[i].author === $('#loggedUser').attr('idUser') || post[i].description.indexOf($('#loggedUser').val()+" wrote to") > -1){
          heading.append(deleteButton).append(author).append(postDate);
        } else {
          heading.append(author).append(date);
        }
        container.append(heading).append(text).append(likeButton);
        posts.prepend(container);
      }
    } else {
      var head = $('#back'),
        follow, unfollow,
        removeFriendButton = $('<div class="navbar-right" id="removeFriend"><div id="removeFriendLabel">Remove friend</div><button type=submit class="navbar-right btn btn-danger glyphicon glyphicon-remove" id="removeFriend" data-toggle="tooltip" data-placement="right" title="Remove friend"></button></div>');
      $('#followButton').html('');
      $('#removeFriend').html('');
      if(post.result === 0){
        follow = $('<div class="navbar-right" id="followButton"><div id="followLabel">Follow</div><button type=submit class="navbar-right btn btn-success glyphicon glyphicon-eye-open" id="follow" data-toggle="tooltip" data-placement="right" title="Follow user"></button></div>');
        head.prepend(removeFriendButton).prepend(follow);
      } else {
        unfollow = $('<div class="navbar-right" id="followButton"><div id="followLabel">Unfollow</div><button type=submit class="navbar-right btn btn-danger glyphicon glyphicon-eye-close" id="unfollow" data-toggle="tooltip" data-placement="right" title="Unfollow user"></button></div>');
        head.prepend(removeFriendButton).prepend(unfollow);
      }
      for(i=0; i<post.posts.length; i++){
        if(post.posts[i].likes.length === 0){
          likes = "";
        } else if (post.posts[i].likes.length === 1) {
          likes = "1 like";
        } else {
          likes = post.posts[i].likes.length + " likes";
        }
        container = $('<div class="panel panel-default posts" idPost="'+post.posts[i]._id+'" datePost="'+post.posts[i].date+'" idAuthor="'+post.posts[i].author+'"></div>');
        shareButton = $('<button type=submit class="navbar-right btn btn-default glyphicon glyphicon-share-alt" id="sharePost" data-toggle="tooltip" data-placement="right" title="Share post"></button>');
        deleteButton = $('<button type=submit class="navbar-right btn btn-danger glyphicon glyphicon-trash" id="deletePost" data-toggle="tooltip" data-placement="right" title="Delete post"></button>');
        likeButton = $('<div class="panel-footer" id="likeButton"><button type=submit class="btn btn-info glyphicon glyphicon-thumbs-up" id="likePost"></button><div id="likes">'+likes+'</div></div>');
        heading = $('<div class="panel-heading author"></div>');
        author = $('<div id="author">'+post.posts[i].description+'</div>');
        text = $('<div id="post">'+post.posts[i].text+'</div>');
        date = post.posts[i].date;
        date = date.split('T');
        date[1] = date[1].split('.');
        date = date[0] + " " + date[1][0];
        postDate = $('<div id="date">' + date + '</div>');
        if(post.posts[i].author === $('#loggedUser').attr('idUser') || post.posts[i].description.indexOf($('#loggedUser').val()+" wrote to") > -1){
          heading.append(deleteButton).append(shareButton).append(author).append(postDate);
        } else {
          heading.append(shareButton).append(author).append(date);
        }
        container.append(heading).append(text).append(likeButton);
        posts.prepend(container);
      }
    }
  });

  socket.on('refreshWall', function(walls){
    currentWall = $('#wall-title').attr('idWall');
    if(walls.indexOf(currentWall) > -1){
      if(currentWall === $('#loggedUser').attr('idUser')){
        socket.emit('getMyWallPosts');
      } else {
        socket.emit('getPosts', currentWall);
      }
    }
  });

  socket.on('getMyWall', function(id){
    if($('#wall-title').attr('idWall') === id){
      $('#wall-title').html('<div id="wallUser">' + $('#loggedUser').val() + '</div>');
      $('#wall-title').attr('idWall', id);
      $('#comment').val('');
      socket.emit('getMyWallPosts');
    }
  });

  socket.on('onlineUsers', function(users){
    $('.status').each(function() {
      $(this).find('img').remove();
    });
    for(i=0; i<users.length; i++){
      $('.'+users[i]).find('.status').html('<img id="onlineImg" src="img/bullet_green.png">');
    }
  });
});
