$(document).ready(function () {
  $('#form-login').submit(function (e) {
    e.preventDefault();
    var user = JSON.stringify({
      username : $('#email-login').val(),
      password : $('#password-login').val()
    });
    $.ajax({
        type: 'POST',
        url: '/login',
        data: user,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data, status) {
          //data.message zawiera info o błednym logowaniu
          if(!data.message){
            window.location = data.redirect;
          }
          else{
          }
        },
        error: function (xhr, status, error) {
          $('#error-login').text('Incorrect e-mail or password.');
          $('#error-login').css('display', 'block');
          $('#email-login').val('');
          $('#password-login').val('');
        }
    });
  });

  $('#form-register').validate({
      rules: {
          email: {
              required: true,
              email: true
          },
          password: {
              required: true,
              minlength: 4
          },
          firstName: {
            required: true
          },
          lastName: {
            required: true
          }
      },
      submitHandler: function (form) {
        var user = JSON.stringify({
          email: $('#email-register').val(),
          password: $('#password-register').val(),
          firstName: $('#first-name-register').val(),
          lastName: $('#last-name-register').val()
        });
        $.ajax({
            type: 'POST',
            url: '/register',
            data: user,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data, status) {
              //data.message zawiera info o błednym logowaniu
              if(!data.message){
                window.location = data.redirect;
              }
              else{
                // pokaz informacje na stronie logowania o błędnych danych
              }
            },
            error: function (xhr, status, error) {
              $('#error-register').text('Given email is used.');
              $('#error-register').css('display', 'block');
              $('#email-register').val('');
              $('#password-register').val('');
              $('#first-name-register').val('');
              $('#last-name-register').val('');
            }
        });
        return false; // required to block normal submit since you used ajax
     }
  });
});
