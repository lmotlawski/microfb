var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  passportLocalMongoose = require('passport-local-mongoose');

var User = new Schema({
  email: String,
	firstName: String,
	lastName: String,
  friends: [{
    type: mongoose.Schema.Types.ObjectId
  }],
  following: [{
    type: mongoose.Schema.Types.ObjectId
  }],
  followedBy: [{
    type: mongoose.Schema.Types.ObjectId
  }]
});

User.plugin(passportLocalMongoose,{
  usernameField: 'email'
});

module.exports = mongoose.model('User', User);
