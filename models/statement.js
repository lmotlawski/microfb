var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var Statement = new Schema({
  type: String,
	from: mongoose.Schema.Types.ObjectId,
  fromText: String,
	to: mongoose.Schema.Types.ObjectId
});

module.exports = mongoose.model('Statement', Statement);
