var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var Message = new Schema({
	from: mongoose.Schema.Types.ObjectId,
	to: mongoose.Schema.Types.ObjectId,
  text: String,
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Message', Message);
