var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var Post = new Schema({
	description: String,
  text: String,
  author: mongoose.Schema.Types.ObjectId,
  date: {
    type: Date,
    default: Date.now
  },
  likes: [{
    type: mongoose.Schema.Types.ObjectId
  }]
});

module.exports = mongoose.model('Post', Post);
